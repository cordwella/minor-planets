#! /usr/bin/env pythons

import sys
import pyds9
import os

from lookup import getnum

CARE_ABOUT_FIELD = False

def view_in_ds9(astid, startrun=None):
    ftrack = open('data/' + astid + '.track.dat')
    print(startrun)
    viewer = pyds9.DS9()
    found_start = False
    field = None
    chip = None
    frames = []
    for line in ftrack:
        frame, _, _, _, _, x, y = line.split()
        if not CARE_ABOUT_FIELD:
            filename = frame_file_name(frame, astid)
            if os.path.isfile(filename):
                frames.append([filename, float(x), float(y)])
            else:
                print("Frame does not exists at {}".format(filename))
        else:
            if get_num(frame) == startrun:
                found_start = True
                chip = get_chip(frame)
                field = get_field(frame)
                filename = frame_file_name(frame, astid)
                if os.path.isfile(filename):
                    frames.append([filename, float(x), float(y)])
                else:
                    print("Frame does not exists at {}".format(filename))

            elif found_start and chip == get_chip(frame) and field == get_field(frame):
                filename = frame_file_name(frame, astid)
                if os.path.isfile(filename):
                    frames.append([filename, float(x), float(y)])
                else:
                    print("Frame does not exists at {}".format(filename))
            elif found_start:
                break

    # Calculate the average x and y
    x_ave = average([x for a,x,b in frames])
    y_ave = average([y for a,x,y in frames])
    viewer.set("blink no")

    viewer.set("frame delete all")
    zoom = "2"

    for filename, x, y in frames:
        viewer.set("frame new")
        viewer.set("fits %s" % filename)
        viewer.set("pan to %s %s physical" % (x, y))
        viewer.set("zoom to 1")
        viewer.set("crosshair %s %s physical" % (x, y))
        # viewer.set("regions command '{crosshai 100 100 20 # color=red}'")
        # TODO: draw region

    # setup blink
    viewer.set("blink yes")
    viewer.set("blink interval 0.5")

def average(l):
    return sum(l)/len(l)

def frame_file_name(fname, astid):
    return os.getcwd() + "/data/" + astid + "/" + fname + ".fit"

def get_chip(f):
    return f.split('.')[0].split('-')[3]

def get_field(f):
    return f.split('.')[0].split('-')[1]

def get_num(f):
    return int(f.split('.')[0].split('-')[0][1:])

if __name__ == '__main__':
    astid = sys.argv[1]
    try:
        startrun = int(sys.argv[2])
    except:
        startrun = None
    view_in_ds9(astid, startrun)
