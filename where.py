#! /usr/bin/env python
#
# Calculate astrometry
#
import sys
import os
import pprint
import math

from geom import DEG2RAD, sex2dec, dec2sex
import datasrc

PARPATH = datasrc.PARPATH

PI = math.pi
HALFPI = 0.5 * PI

subfxy = {}
subfxy['0'] =    0, 1024,    0, 1024
subfxy['1'] =    0, 1024, 1024, 2048
subfxy['2'] =    0, 1024, 2048, 3072
subfxy['3'] =    0, 1024, 3072, 4096
subfxy['4'] = 1024, 2048,    0, 1024
subfxy['5'] = 1024, 2048, 1024, 2048
subfxy['6'] = 1024, 2048, 2048, 3072
subfxy['7'] = 1024, 2048, 3072, 4096
subframes = list(subfxy.keys())
subframes.sort()


def which_subf(x, y):
    for subf in subfxy.keys():
        x1, x2, y1, y2 = subfxy[subf]
        if x >= x1 and x <= x2 and y >= y1 and y <= y2:
            return subf

    #print "Couldn't subframe", x, y
    raise ValueError()
    return None


def nearest_subf(x, y):

    nearest = None
    sepmin = 100000.0
    for subf in subfxy.keys():
        x1, x2, y1, y2 = subfxy[subf]
        xc = 0.5 * (x1 + x2)
        yc = 0.5 * (y1 + y2)
        dx = x - xc
        dy = y - yc
        sep = math.sqrt(dx * dx + dy * dy)
        if sep < sepmin:
            sepmin = sep
            nearest = subf

    return nearest


def rad_to_arcsec(x):
    return 648000 * x / PI


def arcsec_to_rad(x):
    return x * PI / 648000


def complement(a):
    return HALFPI - a


class ParSet:
    def __init__(self, ra, dec, sx, sy, x, y, mu, nu, phi, psi):

        self.ra = ra
        self.dec = dec
        self.sx = sx
        self.sy = sy
        self.x = x
        self.y = y
        self.mu = mu
        self.nu = nu
        self.phi = phi
        self.psi = psi

        self.p = complement(self.dec)

        self.cosphi = math.cos(phi)
        self.sinphi = math.sin(phi)
        self.cospsi = math.cos(psi)
        self.sinpsi = math.sin(psi)

        self.cosphipsi = math.cos(phi - psi)
        self.sinphipsi = math.sin(phi - psi)


def read_par(parfile):

    fin = open(parfile)
    lines = fin.readlines()
    fin.close()

    par = ParSet(
        15.0 * sex2dec(lines[0]) * DEG2RAD,
        sex2dec(lines[1]) * DEG2RAD,
        int(lines[2]),
        int(lines[3]),
        float(lines[4]),
        float(lines[5]),
        float(lines[6]),
        float(lines[7]),
        float(lines[8]) * DEG2RAD,
        float(lines[9]) * DEG2RAD
        )

    return par


def get_par_byname(field, chip, subf):

    parname = '-'.join([field, chip, subf]) + '.par'
    parfile = os.path.join(PARPATH, parname)
    return read_par(parfile)


def sphtriangle(a, b, gamma):

    cosa = math.cos(a)
    sina = math.sin(a)
    cosb = math.cos(b)
    sinb = math.sin(b)
    cosg = math.cos(gamma)
    sing = math.sin(gamma)

    cosc =          cosa * cosb + sina * sinb * cosg
    sinc_sinalpha = sina * sing
    sinc_cosalpha = cosa * sinb - sina * cosb * cosg

    sinc_sinbeta = sinb*sing
    sinc_cosbeta = cosb*sina - sinb*cosa*cosg

    sinc = math.sqrt(sinc_sinalpha*sinc_sinalpha + sinc_cosalpha*sinc_cosalpha)

    c = math.atan2(sinc, cosc)
    alpha = math.atan2(sinc_sinalpha, sinc_cosalpha)
    beta = math.atan2(sinc_sinbeta, sinc_cosbeta)

    return c, alpha, beta


def sky_to_plane(ra, dec, par):

    dra = ra - par.ra
    p = complement(dec)
    rho, theta, g = sphtriangle(p,par.p,dra)
    r = rad_to_arcsec(math.tan(rho))
    xi = r * math.sin(theta)
    eta = r * math.cos(theta)

    return xi, eta


def plane_to_sky(xi, eta, par):

    r = math.sqrt(xi * xi + eta * eta)

    if xi == 0 and eta == 0:
        theta = 0
    else:
        theta = math.atan2(xi, eta)

    rho = math.atan(arcsec_to_rad(r))
    p, dra, g = sphtriangle(rho, par.p, theta)

    dec = complement(p)
    ra = par.ra + dra

    return ra, dec


def sky_to_ccd(par, ra, dec):
    '''Input ra and dec in decimal degrees'''

    xi, eta = sky_to_plane(ra * DEG2RAD, dec * DEG2RAD, par)

    u =  eta * par.cosphi - xi * par.sinphi
    v =  eta * par.sinpsi + xi * par.cospsi

    x = par.sx * u / par.mu + par.x
    y = par.sy * v / par.nu + par.y

    return x, y


def ccd_to_sky(par, x, y):
    '''Return ra and dec in decimal degrees'''

    u = par.sx * par.mu * (x - par.x)
    v = par.sy * par.nu * (y - par.y)

    eta = ( u * par.cospsi + v * par.sinphi) / par.cosphipsi
    xi  = (-u * par.sinpsi + v * par.cosphi) / par.cosphipsi
    ra, dec = plane_to_sky(xi, eta, par)

    return ra / DEG2RAD, dec / DEG2RAD


def test_moa_field(field, chip, ra, dec, verbose=True):

    npixx = 2048
    npixy = 4096
    hw = 50
    x1 = -hw
    y1 = -hw
    x2 = npixx + hw
    y2 = npixy + hw

    partest = get_par_byname(field, chip, '5')
    xest, yest = sky_to_ccd(partest, ra, dec)
    print('##', xest, yest, ra, dec)

    res = None
    if (xest > x1 and xest < x2 and yest > y1 and yest < y2):

        for subf in subframes:

            par = get_par_byname(field, chip, subf)
            x, y = sky_to_ccd(par, ra, dec)
            subf1 = which_subf(xest, yest)
            subf2 = nearest_subf(xest, yest)

            xf = ('%10.4f' % x).lstrip()
            yf = ('%10.4f' % y).lstrip()

            if subf1 == subf:
                res = field, chip, x, y, subf

            if verbose:
                print(field, chip, xf, yf, subf, subf1, subf2)
                if subf1 == subf:
                    print('<---')
                else:
                    print('')

    return res


def find_cooridinates_from_chip_position(field, chip, x, y):

    subf = which_subf(x, y)
    par = get_par_byname(field, chip, subf)
    ra, dec = ccd_to_sky(par, x, y)

    return ra, dec


if __name__ == '__main__':

    field = sys.argv[1]
    chip = sys.argv[2]
    x = float(sys.argv[3])
    y = float(sys.argv[4])
    ra, dec = find_cooridinates_from_chip_position(field, chip, x, y)
    print(ra, dec)
    print(dec2sex(ra/15), dec2sex(dec))
