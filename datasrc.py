try:
    # TODO(amelia): refactor, but like refactor all of the file access
    # Ignore the database if it is not installed
    import boto.s3.connection
    from credentials import ACCESS_KEY, SECRET_KEY, S3_HOST, BUCKET
except Exception:
    pass

#
# Change these according to your system
#
#
# Astrometric calibration parameters - data included with this distribution
# PARPATH = '/home/iabond/Work/asteroids/par'
PARPATH = '/home/amelia/Documents/summerresearch/iabond-asteroids/par'


# Objects picked up on difference image frames
DIOBJP  = '/mnt/sata1/iabond/MOA2proc/diphot'


# Metadata
INFOP   = '/home/amelia/Documents/summerresearch/database/data/GB/info'


# Connection to the database
DIOBJP_DB = ''#

try:
    s3 = boto.connect_s3(
        aws_access_key_id=ACCESS_KEY,
        aws_secret_access_key=SECRET_KEY,
        host=S3_HOST,
        is_secure=True,
        validate_certs=False,
        calling_format=boto.s3.connection.OrdinaryCallingFormat(),
    )

    MOA_DB = s3.get_bucket(BUCKET)
except Exception:
    MOA_DB = None
