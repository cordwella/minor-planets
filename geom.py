#! /usr/bin/env python3
#

#NOTE(amelia): All this is doigng is maths, util file
import sys
import math

DEG2RAD = math.pi / 180.0


def to_cartesian(rad, decd):
    ra = rad * DEG2RAD
    dec = decd * DEG2RAD
    rxy = math.cos(dec)
    x = rxy * math.cos(ra)
    y = rxy * math.sin(ra)
    z = math.sin(dec)
    return x,y,z


def angsep(ra1, dec1, ra2, dec2):
    x1, y1, z1 = to_cartesian(ra1, dec1)
    x2, y2, z2 = to_cartesian(ra2, dec2)
    dotp = x1 * x2 + y1 * y2 + z1 * z2
    return math.acos(dotp) / DEG2RAD


def sex2dec(dms):
    h, m, s = dms.split(':')

    if h.startswith('-'):
        return -(-float(h) + (float(m) + float(s)/60.0)/60.0)
    else:
        return float(h) + (float(m) + float(s)/60.0)/60.0


def dec2sex(deg):

    if deg >= 0:
        sign = '+'
    else:
        sign = '-'
    deg = math.fabs(deg)

    # Number of degrees
    d = int(deg)

    # Number of minutes
    fmin = 60 * (deg - d)
    m = int(fmin)

    # Number of seconds
    s = 60 * (fmin - m)
    fsec = ("%5.2f" % s).lstrip()
    if s < 10:
        fsec = '0' + fsec

    # Format with leading zeros
    if m < 10:
        fmin = '0' + str(m)
    else:
        fmin = str(m)

    dms = ':'.join([sign+str(d), fmin, fsec])
    return dms


if __name__ == '__main__':
    ra = float(sys.argv[1])
    dec = float(sys.argv[2])
    print(dec2sex(ra / 15.0))
    print(dec2sex(dec))
