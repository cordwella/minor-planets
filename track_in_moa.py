#! /usr/bin/env python
#
'''
Track a known asteroid through the MOA exposures
'''
#
import sys
import math

import geom
from where import find_cooridinates_from_chip_position
import jdate
from astcoord import get_asteroid_cooridinates
import where_in_moa
import lookup

NUM1 = 300
NUM2 = 59999
COLOURS = ['R', 'V']


def jd2daynum(jd):
    #     return int(math.floor(jd + 0.5)) - 2450000
    return math.floor(jd)


def map_corners():
    corners = {}
    for i in range(1, 24):
        field = 'gb' + str(i)
        ra1, dec1 = find_cooridinates_from_chip_position(field, '5', 0, 0)
        ra2, dec2 = find_cooridinates_from_chip_position(field, '10', 0, 0)
        corners[field] = ra1, ra2, dec1, dec2
    return corners


def which_fields(corners, ra, dec):
    hits = []
    for field in corners.keys():
        r1, r2, d1, d2 = corners[field]
        if ra >= r1 and ra <= r2 and dec >= d1 and dec <= d2:
            hits.append(field)
    return hits


def map_days(fephem):

    # First get the corners of each field in ra and dec
    corners = map_corners()

    # Test each ephemeris point
    offsets = [-1, 0, 1]
    mapping = {}
    fin = open(fephem)

    for line in fin:
        if line.startswith('#'):
            continue
        items = line.split()
        if len(items) == 0:
            continue
        ras = ':'.join(items[4:7])
        decs = ':'.join(items[7:10])
        ra = 15.0 * geom.sex2dec(ras)
        dec = geom.sex2dec(decs)
        year, month, day = items[0:3]
        jdUT = jdate.cal2jd(year, month, day, 0, 0, 0) - 0.5

        daynum = jd2daynum(jdUT)
        hits = which_fields(corners, ra, dec)
        if len(hits) > 0:
            for dd in offsets:
                d = daynum + dd
                if d not in mapping:
                    mapping[d] = []
                mapping[d].extend(hits)
    fin.close()
    return mapping


def info_OK(finfo):
    if finfo.startswith('B') and not finfo.startswith('BC'):
        try:
            run, field, colour = finfo.split('.')[0].split('-')
            return True
        except ValueError as e:
            print("Issue {} with file {}".format(e, finfo))
            return False
    else:
        return False


def map_ephem_to_exposures(astid):
    fephem = 'data/' + astid + '.ephem.dat'
    daymap = map_days(fephem)

    ftrack = 'data/' + astid + '.track.dat'
    print('# Tracking in MOA to:', ftrack, '...')
    fout = open(ftrack, 'w')

    infofiles = lookup.getinfos()

    fmt = '{:18s} {:18.6f} {:8.2f} {:12.8f} {:12.8f} {:12.4f} {:12.4f}'
    for finfo in infofiles:
        if not info_OK(finfo):
            continue
        run, field, colour = finfo.split('.')[0].split('-')

        # Observation JD and exposure time
        jdstart, expt = lookup.getdate(finfo)

        daynum = jd2daynum(jdstart)
        # Means the asteroid COULD be in this exposure
        if daynum in daymap and field in daymap[daynum]:

            # MPC asteroid coordinates at the time of THIS exposure
            ra, dec = get_asteroid_cooridinates(
                astid, jdstart + 0.5 * expt/86400.0)

            # Precise CCD position
            chip, subf, x, y, = where_in_moa.byfield(field, ra, dec)

            if x >= 0 and y >= 0:
                frame = '-'.join([run, field, colour, chip])
                outs = fmt.format(frame, jdstart, expt, ra, dec, x, y)
                fout.write(outs + '\n')
                print(outs)
    fout.close()


if __name__ == '__main__':
    astid = sys.argv[1]
    map_ephem_to_exposures(astid)
