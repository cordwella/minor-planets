#! /usr/bin/env python3


"""
Download frames for a given asteriod

Will save into the data/{asteriod_id} directory
Will not download data that already exists
Will flatten the directory structure
Will only download R bands (currently)
"""
from datasrc import MOA_DB
import sys


def download_asteriod_frames(asteriod_id):
    # Get frame numbers from the track.dat file

    trackf = open('data/' + asteriod_id + '.track.dat')
    folder = "data/{}/".format(asteriod_id)

    for line in trackf:
        frame = line.split(" ")[0]
        download_frame(frame, folder)


def download_frame(frame, savefolder):
    # format B23676-gb21-R-3
    # database format GB/GB2303/R/B23676-gb21-R-3.fit

    run, field, filter, chip = frame.split('-')

    if field.startswith('gb'):
        galaxy = 'GB'
    else:
        galaxy = field[:3].upper()

    folder = "{}{:02d}{:02d}".format(
        galaxy.upper(), int(field[2:]), int(chip))

    filename = "{}/fit/{}/{}/{}.fit".format(galaxy, folder, filter, frame)
    key = MOA_DB.get_key(filename)
    if key:
        key.get_contents_to_filename("{}{}.fit".format(savefolder, frame))
    else:
        print("Could not find {}".format(filename))

    pass


if __name__ == '__main__':
    astid = sys.argv[1]
    download_asteriod_frames(astid)
