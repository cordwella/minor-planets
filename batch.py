#! /usr/bin/env python3
#

import make_ephem
import draw_fields
import track_in_moa
import crossref
import download_frames

idlist = ['2', '3', '4', '5', '6', '110', '155', '160', '162',
          '170', '172', '198', '200']

for astid in idlist:

    make_ephem.write_asteriod_ephemeris(astid)
    draw_fields.draw_traj_and_field(astid, interactive=False)
    track_in_moa.map_ephem_to_exposures(astid)
    # crossref.crossref_asteriod_and_difference_images(astid)
    download_frames.download_asteriod_frames(astid)
