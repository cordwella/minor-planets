#! /usr/bin/env python3
#
# Locate a record in the object list data
# NOTE(amelia): for a given image frame, ie specific image? check if there
# are records of images from the difference images within that spot
# h is the peak difference imaging flux


import os
import sys
import math

import datasrc

DIOBJP = datasrc.DIOBJP
MOA_DB = datasrc.MOA_DB


def locate(data, x0, y0, tol):
    '''Find all objects within tol pixel radius of x0, y0'''
    records = []
    tol2 = tol * tol
    for line in data.split("\n"):
        if line.startswith('#') or line == '':
            continue
        items = line.split()
        idnum = items[0]
        x = float(items[1])
        y = float(items[2])
        h = float(items[3])
        dx = x - x0
        dy = y - y0
        r2 = dx * dx + dy * dy
        if r2 < tol2:
            r = math.sqrt(r2)
            records.append((idnum, x, y, h, r))
    if not records:
        print("Could not find")
    else:
        print("Could find")
    return records


def find_objects_in_difference_image(frame, x0, y0, tol=5.0):
    filename = os.path.join(DIOBJP, frame + '.dobj.dat')
    if os.path.exists(filename):
        f = open(filename)
        data = f.read()
        f.close()
        return locate(data, x0, y0, tol)
    run, field, filter, chip = frame.split('-')

    folder = ""
    if field.startswith('gb'):
        folder = "GB{:02d}{:02d}".format(int(field[2:]), int(chip))
    else:
        galaxy = field[:3]
        folder = "{}{:02d}{:02d}".format(
            galaxy.upper(), int(field[2:]), int(chip))
    filename = "diphot/{}/{}/{}.dobj.dat".format(folder, filter, frame)
    key = MOA_DB.get_key(filename)
    if key:
        data = key.get_contents_as_string().decode('utf-8')
        return locate(data, x0, y0, tol)
    print("Could not find " + filename)
    return []


if __name__ == '__main__':
    frame = sys.argv[1]
    x0 = float(sys.argv[2])
    y0 = float(sys.argv[3])
    recs = find_objects_in_difference_image(frame, x0, y0)
    print(recs)
