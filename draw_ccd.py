#! /usr/bin/env python3
#
import sys
# import os
# import numpy
import matplotlib.pyplot as plt

# import where
# import geom
# from draw_fields import draw_field

npixx = 2048.0
npixy = 4096.0


def draw_track_on_single_ccd(astid, subf_req):
    # Will draw the track of the asteroid on all parts for a ccd
    # for all fields which it matches

    ftrack = 'data/' + astid + '.track.dat'

    xp = []
    yp = []
    fieldsused = []
    fin = open(ftrack)
    for line in fin:
        items = line.split()
        frame = items[0]
        if frame.endswith(subf_req):
            # run, field, colour, chip = frame.split('-')
            xccd = float(items[5])
            yccd = float(items[6])
            xp.append(xccd)
            yp.append(yccd)
    fin.close()
    print('#', len(xp), 'potential measurements')

    plt.plot(xp, yp, 'b.')

    plt.title(astid + ' / ' + subf_req)
    plt.xlabel('XCCD')
    plt.ylabel('YCCD')

    plt.xlim(0, npixx)
    plt.ylim(0, npixy)
    plt.axes().set_aspect('equal')
    plt.show()


if __name__ == '__main__':

    astid = sys.argv[1]
    subf_req = sys.argv[2]
    draw_track_on_single_ccd(astid, subf_req)
