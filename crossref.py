#! /usr/bin/env python3
#
'''
Cross reference predicted asteroid positions with actual objects found on
the difference images
'''

import sys

from find import find_objects_in_difference_image


def crossref_asteriod_and_difference_images(astid):

    fmt = '{:>8s} {:12.4f} {:12.4f} {:8.1f} {:16.10f}\n'
    ftrack = 'data/' + astid + '.track.dat'
    fxref = 'data/' + astid + '.xref.dat'
    fin = open(ftrack)
    fout = open(fxref, 'w')
    for line in fin:
        items = line.split()
        frame = items[0]
        t = float(items[1]) - 2450000
        x = float(items[5])
        y = float(items[6])
        recs = find_objects_in_difference_image(frame, x, y, tol=5)
        for rec in recs:
            fout.write('{:<16s}'.format(frame) + fmt.format(*rec))
    fin.close()
    fout.close()
    print('# Cross referenced positions in:', fxref)


if __name__ == '__main__':

    astid = sys.argv[1]
    crossref_asteriod_and_difference_images(astid)
