#! /usr/bin/env python3
#
# NOTE(amelia): I have no clue what this does,
# Seems to possibly be mapping a list of objects against a
# list of trans neptunian objects

def get_list(filename):
    mapping = {}
    fin = open(filename)
    for line in fin:
        items = line.split()
        if len(items) == 0:
            continue
        idnum = items[0]
        mapping[idnum] = line
    fin.close()
    return mapping


def main(filename):
    mapping = get_list('list.txt')
    # print mapping.keys()
    tag1 = '<td align="right"><span>'
    tag2 = '</span></td>'
    fin = open(filename)
    for line in fin:
        if line.startswith(tag1):
            idnum = '(' + line.lstrip(tag1).rstrip('\n').rstrip(tag2) +')'
            if mapping.has_key(idnum):
                print(mapping[idnum])
    fin.close()


if __name__ == '__main__':
    main('tnos.html')
