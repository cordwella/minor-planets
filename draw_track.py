#! /usr/bin/env python3
#
'''
Plot on the MOA sky, asteroid position for each exposure that it is in
'''
import sys
import pylab

from draw_fields import draw_field


def draw_asteroid_on_exposures(astid):

    ftrack = 'data/' + astid + '.track.dat'

    xp = []
    yp = []
    fieldsused = []
    fin = open(ftrack)
    for line in fin:
        items = line.split()
        frame = items[0]
        run, field, colour, chip = frame.split('-')
        if field not in fieldsused:
            fieldsused.append(field)
        ra = float(items[3])
        dec = float(items[4])
        xp.append(ra)
        yp.append(dec)
    fin.close()
    print('#', len(xp), 'potential measurements')

    for field in fieldsused:
        draw_field(field)
    pylab.plot(xp, yp, 'b.')

    pylab.title(astid)
    pylab.xlabel('RA / deg')
    pylab.ylabel('Dec / deg')

    pylab.xlim(280, 263)
    pylab.ylim(-36, -20)
    pylab.savefig(astid + '.png')
    pylab.show()


if __name__ == '__main__':

    astid = sys.argv[1]
    draw_asteroid_on_exposures(astid)
