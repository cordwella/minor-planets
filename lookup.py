#! /usr/bin/env python3
'''
Look up metadata info. Here it is done the simple-minded way by just reading
from flat file storage rather than a database.
'''
import os

import datasrc

# Meta data files are located here
INFOP = datasrc.INFOP


def getnum(x):
    # NOTE(amelia): Get the run number from the file name
    try:
        num = int(x.split('.')[0].split('-')[0][1:])
    except ValueError:
        num = 0
    return num


def getinfos():
    infofiles = os.listdir(INFOP)
    infofiles.sort(key=lambda x: getnum(x))
    return infofiles


def getdate(finfo):
    jd1 = 0.0
    expt = 0.0
    fin = open(os.path.join(INFOP, finfo))
    for line in fin:
        items = line.split()
        if line.startswith('JDSTART'):
            jd1 = float(items[2])
        elif line.startswith('EXPTIME'):
            expt = float(items[2])
    fin.close()
    return jd1, expt
