#! /usr/bin/env python3
#
'''
Generate ephemeris data for a known asteroid
'''
import sys

import mpconnect


def write_asteriod_ephemeris(astid):

    fephem = 'data/' + astid + '.ephem.dat'
    year1 = 2004
    year2 = 2010

    # Get ephemerides for each night at midnight in NZ
    datestart = '2014-01-01'
    utoff = 12.0
    ndays = 365
    daystep = 1

    fout = open(fephem, 'w')
    for year in range(year1, year2 + 1):
        print('#', astid, year, '...')
        datestart = str(year) + '-01-01'
        res = mpconnect.mpeph(astid, datestart, utoff, ndays, daystep)
        lines = res.split('\n')
        for line in lines:
            items = line.split('<')
            outs = items[0]
            if len(outs) > 0:
                fout.write(outs + '\n')
    fout.close()

    print('# Ephemerides from', year1, 'to', year2, 'in', fephem)


if __name__ == '__main__':

    astids = sys.argv
    astids.pop(0)
    for astid in astids:
        write_asteriod_ephemeris(astid)
